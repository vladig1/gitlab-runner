### How to add and register a new Shared GitLab Runner?

1. Log into your GitLab account on (https://gitlab.com)
2. Go to `Groups`
3. Select the desired group from the list of groups (e.g., `vladig`)
4. Expand `Build` section from the menu to the left and select `Runners`
5. Select `New group runner`
6. Fill in the fields:
   1. Platform: `Linux`
   2. Tags: `shell` (may also check `run untagged`, but it is only optional)
   3. The rest of the fields are optional by design, so skipped them.
7. Click `Create runner`
8. Copy the `gitlab-runner register` command from Step 1
9. Enter your gitlab-runner container: `docker exec -it <gitlab-runner-container-name> /bin/bash`
10. Inside the container run the copied `gitlab-runner register` command from above (Step 8): (e.g.,`gitlab-runner register  --url <gitlab-instance-url>  --token <auth-token>`)
11. Answer the prompt questions to complete the gitlab-runner registration:
    1.  URL: `https://gitlab.com` or the URL to your GitLab instance (if self-hosted)
    2.  Choose an executor: `shell` if you want to run shell commands
12. Test if runner is successfully registered by running `gitlab-runner run` inside the gitlab-runner container.
13. Go to `https://gitlab.com/groups/<group-name>/-/runners` (e.g., `https://gitlab.com/groups/vladig1/-/runners`) to check the status of your registered gitlab-runners.